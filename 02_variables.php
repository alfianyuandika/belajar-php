<?php

// What is a variable

// Variable types
/*
    String
    Integer
    Float/Double
    Boolean
    Null
    Array
    Object
    Resource
*/

// Declare variables
$name = "Alfian";
$age = 28;
$isMale = true;
$height = 1.72;
$salary = null;

// Print the variables. Explain what is concatenation
echo $name . "<br>";
echo $age . "<br>";
echo $isMale . "<br>";
echo $height . "<br>";
echo $salary . "<br>";

// Print types of the variables
echo gettype($name) . '<br>';
echo gettype($age) . '<br>';
echo gettype($isMale) . '<br>';
echo gettype($height) . '<br>';
echo gettype($salary) . '<br>';

// Print the whole variable
echo "<br>";
var_dump($name, $age, $isMale, $salary, $height);
echo "<br>";

// Change the value of the variable
$name = false;

// Print type of the variable
echo "<br>";
echo gettype($name) . "<br>";;


// Variable checking functions
echo "<br>"; 
echo "Variable checking functions". "<br>"; 
echo "isName" . is_string($name) . "<br>"; // false
echo "isInt" . is_int($age) . "<br>";  // true
echo "isBool" . is_bool($isMale) . "<br>";  // true
echo "isDouble" . is_double($height) . "<br>";  // true

// Check if variable is defined
echo "<br>"; 
echo "if variable is defined". "<br>"; 
echo isset($name) . "<br>";
echo isset($address) . "<br>";

// Constants
echo "<br>"; 
define("PI", 3.14);
echo PI . "<br>";

// Using PHP built-in constants
echo SORT_ASC . '<br>';
echo SORT_DESC . '<br>';
echo PHP_INT_MAX . '<br>';
